# QA Challenge - Solution

Hi! It was a great challenge no matter the result I get. I could learn a lot about container and drag and drop with Protractor. Thank you for the opportunity and time :) 

## Set up

Install dependences

```
npm install
```

Check if Protractor is correctly installed

```
protractor --version
```

If not, install it and then check instalation again

```
npm install -g protractor
```

Download and update browser binaries

```
webdriver-manager update
```

**Make sure the page which tests are performed against is up by following the README file of [qa-sortable-challenge](https://bitbucket.org/mindera/qa-sortable-challenge/src/master/).**

## Running tests :)

To run tests Protractor reads conf.js file, where all test specification go. This way Protractor knows what to execute and how. In conf.js file it is possible to set test configuration like browser or browsers that will be used, how many instances of it will run, browser capabilities like window size, safe-mode and others. Also, it's the place tests can be organized in suites by grouping spec files path.

```
protractor conf.js --suite test
```

The result and duration of each test will can be seen on console output.

### Built With

* [Protractor](https://www.protractortest.org) - End-to-end test framework
* [jasmine-spec-reporter](https://www.npmjs.com/package/jasmine-spec-reporter) - Real time console spec reporter for jasmine testing framework.
* [NPM](https://www.npmjs.com) - Dependency Management

### Versions used in this test

* Protractor version 5.4.1 
* Selenium standalone version 3.141.59 `$ webdriver-manager status`
* Chromedriver version 2.29 `$ webdriver-manager status`
* Google Chrome version 64.0.3282.167 (Official Build) (64-bit)
* Jasmine-spec-reporter version 4.2.1
* OS Ubuntu 16.04 LTS

## Check here

* Any question about the structure or framework adopted please feel free to contact me.
* If tests don't run for any reason, please contact me so I can help. It can happen due to driver, browser or protractor version conflict.
* **Skype: gianegianotto**
* **Telemóvel: 936 739 154**
* **Email: gianegf@gmail.com**
